package com.radios.india.extra

enum class PlaybackStatus {
    PLAYING,
    PAUSED
}
