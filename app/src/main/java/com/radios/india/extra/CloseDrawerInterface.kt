package com.radios.india.extra

/**
 * Created by Mohsin on 5/30/2018.
 */

interface CloseDrawerInterface {
    fun close()
}
