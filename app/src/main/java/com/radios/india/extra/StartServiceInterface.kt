package com.radios.india.extra

/**
 * Created by Mohsin on 6/1/2018.
 */

interface StartServiceInterface {
    fun startServiceFromMain()
}
