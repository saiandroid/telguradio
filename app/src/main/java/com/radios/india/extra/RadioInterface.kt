package com.radios.india.extra

/**
 * Created by Mohsin on 5/28/2018.
 */

interface RadioInterface {
    fun play()
    //fun stop()
    fun pause()
    operator fun next()
    fun prev()
    fun buffering()
    fun stopBuffering()
    fun error()
}
