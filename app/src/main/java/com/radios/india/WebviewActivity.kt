package com.radios.india

import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar

class WebviewActivity : AppCompatActivity() {

    private var webView: WebView? = null
    private var progress: ProgressBar?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        // add back arrow to toolbar
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        progress=findViewById<ProgressBar>(R.id.progress)
        webView = findViewById<WebView>(R.id.webView)
//        val webSettings = webView!!.getSettings()
        //webSettings.setPluginState(WebSettings.PluginState.ON)
        //webSettings.setJavaScriptEnabled(true)
        //webSettings.setUseWideViewPort(true)
        //webSettings.setLoadWithOverviewMode(true)

        progress!!.visibility= View.VISIBLE

        if (android.os.Build.VERSION.SDK_INT >= 24) {
            webView!!.webViewClient = object : WebViewClient() {

                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    progress!!.visibility=View.VISIBLE
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    progress!!.visibility=View.GONE
                }

            }
        } else {
            webView!!.webViewClient = object : WebViewClient() {

                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    progress!!.visibility=View.VISIBLE
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    progress!!.visibility=View.GONE
                }
            }
        }
        if (intent.getStringExtra("page") == "disc"){
            supportActionBar!!.title = getString(R.string.disclaimer)
            webView!!.loadUrl("https://radiosindia.net/telugu/disclaimer.html")
        } else if(intent.getStringExtra("page") == "help"){
            supportActionBar!!.title = "Help / FAQ"
            webView!!.loadUrl("https://radiosindia.net/telugu/help.html")
        }
        else if(intent.getStringExtra("page") == "privacy"){
            supportActionBar!!.title = "Privacy Policy"
            webView!!.loadUrl("https://radiosindia.net/telugu/privacypolicy.html")
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
