package com.radios.india

import android.app.Activity
import android.content.*
import android.graphics.Color
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.PersistableBundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.updateLayoutParams
import com.google.android.exoplayer2.util.Util
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.radios.india.databinding.ActivityMainBinding
import com.radios.india.databinding.ActivityRadioStationBinding
import com.radios.india.extra.RadioInterface
import com.radios.india.models.CatListModel
import com.radios.india.models.CatListModel.Companion.CH_URL
import com.radios.india.models.CatListModel.Companion.CH_URL_2
import com.radios.india.service.MediaPlayerService
import com.radios.india.utils.Config
import com.radios.india.view.CircularSeekBar
import com.squareup.picasso.Picasso
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class RadioStationActivity : AppCompatActivity(), RadioInterface {

    private var player: MediaPlayerService? = null
    internal var serviceBound = false
    private var play: ImageView? = null
    private var iv_fmstation: ImageView? = null
    private var fav: ImageView? = null
    private var activeAudio: CatListModel? = null
    private var tv_radio_name: TextView? = null
    private var tv_current_song: TextView? = null
    private var smoothProgressBar: SmoothProgressBar? = null
    private var circularSeekBar: CircularSeekBar? = null
    private var audio: AudioManager? = null
    private var sharedPreferences: SharedPreferences? = null
    private var jsonArray = JSONArray()
    private var fav_flag = false
    private var mAdView: AdView? = null
    private var mInterstitialAd: InterstitialAd? = null
    private var sleep_timer: ImageView? = null
    private val handler = Handler()
    private var tv_sleep_time: TextView? = null;
    private var buttonSetSleepTimer: Button? = null;
    private lateinit var binding: ActivityRadioStationBinding
    private lateinit var nativeAdContainer: FrameLayout
    private var nativeAd: NativeAd? = null
    private val TAG = "RadioStationActivity"
    private val runnable = Runnable {
        if (player != null && player!!.isPlaying) {
            player!!.onPause()
            play!!.setImageResource(R.drawable.ic_play)
            sleep_timer!!.setImageResource(R.drawable.ic_sleep_timer)
            sharedPreferences!!.edit().putInt("sleepTime", 0).apply()
            player?.stopSelf()
            finishAffinity()
        }
    }

    //Binding this Client to the AudioPlayer Service
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) { // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as MediaPlayerService.LocalBinder
            player = binder.service
            serviceBound = true
            player?.let { player ->
                if (player.isPlaying && (player.currentPlayingURL == activeAudio?.chUrl) || (player.currentPlayingURL == activeAudio?.chUrl2)) {
                    stopBuffering()
                    player.updateData()
                } else {
                    if (!fromNotif) {
                        player.playNewAudio()
                        player.updateData()
                    } else {
                        if (!player.isPlaying) {
                            play?.setImageResource(R.drawable.ic_play)
                        }
                        player.updateData()
                    }
                }
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            serviceBound = false
        }

    }

    val audioList: ArrayList<CatListModel>?
        get() {
            val gson = Gson()
            val type = object : TypeToken<ArrayList<CatListModel>>() {

            }.type
            return gson.fromJson<ArrayList<CatListModel>>(Config.itemArray.toString(), type)
        }

    private fun loadNativeAd() {
        val testId = if(BuildConfig.DEBUG) "ca-app-pub-3940256099942544/1044960115" else getString(R.string.native_ad_unit_id)
        val adLoader = AdLoader.Builder(this, testId)
            .forNativeAd { ad: NativeAd ->
                nativeAd?.destroy()
                nativeAd = ad
                populateNativeAdView(ad)
            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.e(TAG, "Failed to load native ad: ${adError.message}")
                }
            })
            // Set up NativeAdOptions to request a landscape-oriented image asset.
            .withNativeAdOptions(
                NativeAdOptions.Builder()
                .setMediaAspectRatio(NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_LANDSCAPE)
                .build())
            .build()

        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateNativeAdView(nativeAd: NativeAd) {
        // Inflate the ad unified layout.
        val adView = layoutInflater.inflate(R.layout.ad_unified, null) as NativeAdView

        // Set the media view.
        // This is not necessary since we are not implementing a media view
        // adView.setMediaView(adView.findViewById(R.id.ad_media))

        // Set other ad assets.
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)

        // The headline and mediaContent are guaranteed to be in every NativeAd.
        (adView.headlineView as TextView).text = nativeAd.headline

        // Set the other view content.
        if (nativeAd.icon == null) {
            adView.iconView?.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(nativeAd.icon?.drawable)
            adView.iconView?.visibility = View.VISIBLE
        }

        if (nativeAd.advertiser == null) {
            adView.advertiserView?.visibility = View.INVISIBLE
        } else {
            (adView.advertiserView as TextView).text = nativeAd.advertiser
            adView.advertiserView?.visibility = View.VISIBLE
        }

        if (nativeAd.body == null) {
            adView.bodyView?.visibility = View.GONE
        } else {
            adView.bodyView?.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }

        if (nativeAd.callToAction == null) {
            adView.callToActionView?.visibility = View.INVISIBLE
        } else {
            adView.callToActionView?.visibility = View.VISIBLE
            (adView.callToActionView as Button).text = nativeAd.callToAction
        }
        val mediaView = adView.findViewById<MediaView>(R.id.ad_media)
        adView.mediaView = mediaView

        // This method tells the Google Mobile Ads SDK that you have finished populating the
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd)
        adView.mediaView?.setImageScaleType(ImageView.ScaleType.CENTER_CROP)

        // Make the container visible
        nativeAdContainer.visibility = View.VISIBLE
        nativeAdContainer.removeAllViews()
        // Add the Native Ad View to the container
        nativeAdContainer.addView(adView)

    }
    override fun onDestroy() {
        nativeAd?.destroy()
        super.onDestroy()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRadioStationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        nativeAdContainer = findViewById(R.id.native_ad_container)
        loadNativeAd()

        //---------------------  Edge-to-Edge and System Bar Styling -------------------------//
        // Make the app go edge-to-edge
        WindowCompat.setDecorFitsSystemWindows(window, false)
        //Get the instance
        val windowInsetsController =
            WindowInsetsControllerCompat(window, window.decorView)
        windowInsetsController.isAppearanceLightStatusBars = false // Set to true if using a light background
        windowInsetsController.isAppearanceLightNavigationBars = false// Set to true if using a light background

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        enableEdgeToEdge()
        ViewCompat.setOnApplyWindowInsetsListener(binding.nativeAdContainer) { v, windowInsets ->
            val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
            // Apply the insets as a margin to the view. This solution sets
            // only the bottom, left, and right dimensions, but you can apply whichever
            // insets are appropriate to your layout. You can also update the view padding
            // if that's more appropriate.
            v.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                leftMargin = insets.left
                bottomMargin = insets.bottom
                rightMargin = insets.right
            }

            // Return CONSUMED if you don't want want the window insets to keep passing
            // down to descendant views.
            WindowInsetsCompat.CONSUMED
        }
        // add back arrow to toolbar
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        Config.audioList = audioList

        play = findViewById<View>(R.id.play) as ImageView
        fav = findViewById<View>(R.id.fav) as ImageView
        sleep_timer = findViewById<ImageView>(R.id.sleep_timer) as ImageView
        iv_fmstation = findViewById<View>(R.id.iv_fmstation) as ImageView
        tv_radio_name = findViewById<View>(R.id.tv_radio_name) as TextView
        tv_current_song = findViewById<View>(R.id.tv_current_song) as TextView
        smoothProgressBar = findViewById<View>(R.id.smoothProgressBar) as SmoothProgressBar
        sharedPreferences = getSharedPreferences(Config.shared_pref_name, Activity.MODE_PRIVATE)

        circularSeekBar = findViewById<View>(R.id.circularSeekBar) as CircularSeekBar
        circularSeekBar!!.isLockEnabled = true
        audio = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val currentVolume = audio!!.getStreamVolume(AudioManager.STREAM_MUSIC)
        val maxVolume = audio!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        circularSeekBar!!.max = maxVolume
        circularSeekBar!!.progress = currentVolume
        circularSeekBar!!.setOnSeekBarChangeListener(object : CircularSeekBar.OnCircularSeekBarChangeListener {
            override fun onProgressChanged(circularSeekBar: CircularSeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    audio!!.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0)
                }
            }

            override fun onStopTrackingTouch(seekBar: CircularSeekBar) {

            }

            override fun onStartTrackingTouch(seekBar: CircularSeekBar) {

            }
        })

        setData() //
        fav!!.setOnClickListener { changeFav() }

        MediaPlayerService.setInterface(this@RadioStationActivity)
        if (intent.hasExtra("fromService")) {
            fromNotif = true
        }
        if (!serviceBound) { //            if(fromNotif) {
            /*  if (player!!.isPlaying) {
//                player!!.onPause()
                  play!!.setImageResource(R.drawable.ic_pause)
              } else {
//                player!!.onPlay()
                  play!!.setImageResource(R.drawable.ic_play)
              }*/ //            }else {
            play!!.setImageResource(R.drawable.ic_pause)
            playAudio() //            }
        }

        play!!.setOnClickListener {
            if (player!!.isPlaying) {
                player!!.onPause()
                play!!.setImageResource(R.drawable.ic_play)
            } else {
                player!!.onPlay()
                play!!.setImageResource(R.drawable.ic_pause)
            }
        }

        findViewById<View>(R.id.prev).setOnClickListener {
            if (serviceBound) {
                loadNativeAd()
                player?.onSkipToPrevious()
                setData()
            }
        }

        findViewById<View>(R.id.next).setOnClickListener {
            if (serviceBound) {
                loadNativeAd()
                player?.onSkipToNext()
                setData()
            }
        }

        findViewById<ImageView>(R.id.power).setOnClickListener {
            if (player != null) player!!.stopSelf()
            ActivityCompat.finishAffinity(this@RadioStationActivity);
        }

        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView?.loadAd(adRequest)
        mAdView?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mAdView?.visibility = View.VISIBLE
            }

        }

        InterstitialAd.load(this, getString(R.string.interstitial_ad_unit_id), adRequest, callback)

        val currentProgress = sharedPreferences!!.getInt("sleepTime", 0)
        sleep_timer!!.setImageResource(R.drawable.ic_sleep_timer)
        if (currentProgress > 0) {
            sleep_timer!!.setImageResource(R.drawable.ic_sleep_timer_on)
            handler.removeCallbacks(runnable)
            handler.postDelayed(runnable, currentProgress.toLong())
        }

        sleep_timer!!.setOnClickListener {
            showDialog()
        }

    }

    private val callback = object : InterstitialAdLoadCallback() {
        override fun onAdFailedToLoad(adError: LoadAdError) {
            mInterstitialAd = null
        }

        override fun onAdLoaded(interstitialAd: InterstitialAd) {
            mInterstitialAd = interstitialAd
        }

    }
    private var fromNotif = false;

    fun showDialog() {
        val currentProgress = sharedPreferences!!.getInt("sleepTime", 0)
        val popDialog = AlertDialog.Builder(this).create()
        val v = layoutInflater.inflate(R.layout.dialogue_sleep_timer, null)
        tv_sleep_time = v.findViewById(R.id.tv_sleepValue)
        buttonSetSleepTimer = v.findViewById(R.id.but_setSleepTime)
        val butCancelSleepTime = v.findViewById<Button>(R.id.but_cancelSleepTime)
        val btnReset = v.findViewById<Button>(R.id.but_reset)
        val seek = v.findViewById<SeekBar>(R.id.seek_bar)
        if (currentProgress > 0) {
            seek.progress = currentProgress / 60000
            tv_sleep_time!!.text = "" + currentProgress / 60000
        }
        seek.max = 100;
        popDialog.setView(v);

        seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    tv_sleep_time!!.text = "" + progress
                    if (progress == 0) {
                        buttonSetSleepTimer!!.text = getString(R.string.sleep_now)
                    } else {
                        buttonSetSleepTimer!!.text = "Sleep After $progress Mins"
                    }
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })

        popDialog.show()

        butCancelSleepTime.setOnClickListener {
            popDialog.dismiss()
        }

        buttonSetSleepTimer?.setOnClickListener {
            handler.postDelayed(runnable, ((seek.progress * 60000).toLong()))
            sharedPreferences?.edit()?.putInt("sleepTime", seek.progress * 60000)?.apply()
            Toast.makeText(this@RadioStationActivity, "Sleep Timer has been set.", Toast.LENGTH_SHORT).show()
            sleep_timer!!.setImageResource(R.drawable.ic_sleep_timer_on)
            popDialog.dismiss()
        }

        btnReset.setOnClickListener {
            Toast.makeText(this@RadioStationActivity, "Sleep Timer Cancelled.", Toast.LENGTH_SHORT).show()
            sharedPreferences!!.edit().putInt("sleepTime", 0).apply()
            handler.removeCallbacks(runnable)
            sleep_timer!!.setImageResource(R.drawable.ic_sleep_timer)
            popDialog.dismiss()
        }
    }

    private fun getUserAgent(): String {

        return Util.getUserAgent(this, javaClass.simpleName)
    }

    private fun setData() {
        try {
            activeAudio = Config.audioList!![Config.index]
            val transformation = RoundedTransformationBuilder().borderColor(Color.parseColor("#8c8c8c")).borderWidthDp(1f).cornerRadiusDp(30f).oval(false).build()

            //           Picasso.Builder(applicationContext).build()
            Picasso.get().load(activeAudio!!.chImage).placeholder(R.drawable.rescueimg6).error(R.drawable.rescueimg6).fit().transform(transformation).into(iv_fmstation)

            tv_radio_name!!.text = activeAudio!!.chName
            fav_flag = false
            fav!!.setImageResource(R.drawable.ic_mark_fav)
            checkFav()

            tv_current_song!!.text = ""
        } catch (e: Exception) {
            e.printStackTrace()
        }

        /*ShoutCastMetadataRetriever smr = new ShoutCastMetadataRetriever();
        try {
            smr.setDataSource(activeAudio.getChUrl());
            String artist = smr.extractMetadata(ShoutCastMetadataRetriever.METADATA_KEY_ARTIST);
            String title = smr.extractMetadata(ShoutCastMetadataRetriever.METADATA_KEY_TITLE);
            tv_current_song.setText(title);
        } catch (ScrapeException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }*/
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState.putBoolean("serviceStatus", serviceBound)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        serviceBound = savedInstanceState.getBoolean("serviceStatus")
    }


    private fun playAudio() { //Check is service is active
        if (!serviceBound) {
            val playerIntent = Intent(this, MediaPlayerService::class.java) //startService(playerIntent)
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE)
        } else { //Service is active
            //Send a broadcast to the service -> PLAY_NEW_AUDIO
            val broadcastIntent = Intent(Config.Broadcast_PLAY_NEW_AUDIO)
            sendBroadcast(broadcastIntent)
        }
    }

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        if (serviceBound) {
            unbindService(serviceConnection);
            //service is active
            player.stopSelf();
        }
    }*/

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    var cnt = 0;

    override fun onBackPressed() {

        if (fromNotif) {
            super.onBackPressed()
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            fromNotif = false
        } else {
            try {
                    if (mInterstitialAd != null) {
                        mInterstitialAd?.show(this) // return;
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            onBackPressFinishingTask()
            super.onBackPressed()
        }

    }

    private fun onBackPressFinishingTask() {/*  if (Config.fromMain && player!!.isPlaying) {
              Config.fromMain = false
              Config.fromRadio = true
          } else {
              Config.fromMain = false
              Config.fromRadio = false
          }*/

        handler.removeCallbacks(runnable) //handler.removeCallbacks(runnable)
        overridePendingTransition(R.anim.slide_in_top_fade, R.anim.slide_out_bottom_fade)
    }

    private fun checkFav() {
        val str = sharedPreferences!!.getString(Config.shared_fav_list, jsonArray.toString())
        try {
            jsonArray = JSONArray(str)
            val n = jsonArray.length()
            for (i in 0 until n) {
                val jsonObject = jsonArray.getJSONObject(i)
                if (jsonObject.optString(CH_URL) == activeAudio?.chUrl && !fav_flag) {
                    fav_flag = true
                    fav?.setImageResource(R.drawable.ic_mark_fav_selected)
                } else if ((jsonObject.optString(CH_URL_2) == activeAudio?.chUrl2 && !fav_flag)) {
                    fav_flag = true
                    fav?.setImageResource(R.drawable.ic_mark_fav_selected)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun changeFav() {
        if (fav_flag) {
            fav_flag = false
            fav!!.setImageResource(R.drawable.ic_mark_fav)
            changeSharedPref()
        } else {
            fav_flag = true
            fav!!.setImageResource(R.drawable.ic_mark_fav_selected)
            val str = sharedPreferences!!.getString(Config.shared_fav_list, jsonArray.toString())
            try {
                val jsonArray = JSONArray(str)
                val audioJson = Gson().toJson(activeAudio)
                val jsonObject = JSONObject(audioJson)
                jsonArray.put(jsonObject)
                sharedPreferences!!.edit().putString(Config.shared_fav_list, jsonArray.toString()).apply()
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    private fun changeSharedPref() {
        val str = sharedPreferences!!.getString(Config.shared_fav_list, jsonArray.toString())
        try {
            jsonArray = JSONArray(str)
            val n = jsonArray.length()
            for (i in 0 until n) {
                val jsonObject = jsonArray.getJSONObject(i)

                if (jsonObject.optString(CH_URL) == activeAudio?.chUrl || jsonObject.optString(CH_URL_2) == activeAudio?.chUrl2) {
                    jsonArray = removeIndex(jsonArray, i)
                    val applied = sharedPreferences!!.edit().putString(Config.shared_fav_list, jsonArray.toString()).commit()
                    if (applied) {
                        Config.currentDisplayedArray = jsonArray
                    }
                    return
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun removeIndex(jsonArray: JSONArray, index: Int): JSONArray {
        val output = JSONArray()
        val len = jsonArray.length()
        for (i in 0 until len) {
            if (i != index) {
                try {
                    output.put(jsonArray.get(i))
                } catch (e: JSONException) {
                    throw RuntimeException(e)
                }

            }
        }
        return output
    }

    override fun play() {
        play!!.setImageResource(R.drawable.ic_play)
    }

    override fun pause() {
        play!!.setImageResource(R.drawable.ic_pause)
    }

    override fun next() {
        setData()
    }

    override fun prev() {
        setData()
    }

    override fun buffering() {
        smoothProgressBar?.visibility = View.VISIBLE
        if (!Config.isNetworkAvailable(this@RadioStationActivity)) {
            binding.noInternetBar.tvNoInternet.text = getString(R.string.no_network_bar_text)
            findViewById<RelativeLayout>(R.id.no_internet_bar).visibility = View.VISIBLE
        } else {
            findViewById<RelativeLayout>(R.id.no_internet_bar).visibility = View.GONE
        }
    }

    override fun stopBuffering() {
        smoothProgressBar?.visibility = View.GONE
        if (!Config.isNetworkAvailable(this@RadioStationActivity)) {
            binding.noInternetBar.tvNoInternet.text = getString(R.string.no_network_bar_text)
            findViewById<RelativeLayout>(R.id.no_internet_bar).visibility = View.VISIBLE
        } else {
            findViewById<RelativeLayout>(R.id.no_internet_bar).visibility = View.GONE
        }
    }

    override fun error() {
        smoothProgressBar?.visibility = View.GONE
        findViewById<RelativeLayout>(R.id.no_internet_bar).visibility = View.VISIBLE
        binding.noInternetBar.tvNoInternet.text = getString(R.string.current_channel_is_not_available)
        binding.noInternetBar.tvNoInternet.isSelected = true
        if (!Config.isNetworkAvailable(this@RadioStationActivity)) {
            binding.noInternetBar.tvNoInternet.text = getString(R.string.no_network_bar_text)
            findViewById<RelativeLayout>(R.id.no_internet_bar).visibility = View.VISIBLE
        }
    }

    /*override fun stop() {
        if (VERSION.SDK_INT>=16)
            finishAffinity()
        else
            ActivityCompat.finishAffinity(this@RadioStationActivity);
    }*/
}
