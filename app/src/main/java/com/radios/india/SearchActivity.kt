package com.radios.india

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import com.google.android.gms.ads.*
import android.text.Editable
import android.text.TextWatcher
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.radios.india.adapters.SearchAdapter
import com.radios.india.databinding.ActivityRadioStationBinding
import com.radios.india.databinding.ActivitySearchBinding
import com.radios.india.extra.StartServiceInterface
import com.radios.india.service.MediaPlayerService
import com.radios.india.utils.Config
import org.json.JSONArray
import java.util.Locale


class SearchActivity : AppCompatActivity(), StartServiceInterface {

    private var mAdView: AdView? = null
    private var mInterstitialAd: InterstitialAd? = null
    private var jsonArray: JSONArray = JSONArray()
    private var adapter: SearchAdapter? =null
    private lateinit var binding: ActivitySearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        // add back arrow to toolbar
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        binding.search.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.isNotEmpty()){
                    search(s.toString())
                    binding.cross.visibility=View.VISIBLE
                }else{
                    binding.cross.visibility=View.GONE
                }
            }
        })

        binding.cross.setOnClickListener {
            binding.search.setText("")
            binding.cross.visibility = View.GONE
            jsonArray = JSONArray()
            adapter!!.refresh(jsonArray)
        }

        binding.recyclerView.setHasFixedSize(true)
        adapter = SearchAdapter(this@SearchActivity, jsonArray, this)
        binding.recyclerView.layoutManager = LinearLayoutManager(this@SearchActivity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = adapter


        mAdView = findViewById(R.id.adView)

        mAdView?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mAdView!!.visibility = View.VISIBLE
            }
        }
        val adRequest = AdRequest.Builder().build()

        mAdView?.loadAd(adRequest)
//        MobileAds.initialize(this, getString(R.string.interstitial_ad_app_id))
        InterstitialAd.load(this, if(BuildConfig.DEBUG) "ca-app-pub-3940256099942544/1033173712" else
            getString(R.string.interstitial_ad_unit_id), adRequest, callback)

    }
    private val callback = object : InterstitialAdLoadCallback() {
        override fun onAdFailedToLoad(adError: LoadAdError) {
            mInterstitialAd = null
        }

        override fun onAdLoaded(interstitialAd: InterstitialAd) {
            mInterstitialAd = interstitialAd
        }

    }

    private fun search(text: String){
        val n=Config.jsonArray.length()
        jsonArray= JSONArray()
        for (i in 0 until n){
            val jsonObj=Config.jsonArray.getJSONObject(i);
            val catName=jsonObj.getString("cat_name")
            val jsonArray1=jsonObj.getJSONArray("cat_list")
            val len=jsonArray1.length()
            for (j in 0 until len){
                val jsonObj1=jsonArray1.getJSONObject(j)
                if (jsonObj1.getString("ch_name").lowercase(Locale.getDefault()).contains(text.lowercase(Locale.getDefault()))){
                    jsonObj1.put("cat_name", catName)
                    jsonArray.put(jsonObj1)
                }
            }
        }
        adapter!!.refresh(jsonArray)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (mInterstitialAd!=null) {
            if(!Config.exitedMain) {
                mInterstitialAd?.show(SearchActivity@this)
            }
        }
        overridePendingTransition(R.anim.slide_in_top_fade, R.anim.slide_out_bottom_fade)
    }

    override fun startServiceFromMain() {
        val playerIntent = Intent(this, MediaPlayerService::class.java)
        startService(playerIntent)
    }
}
