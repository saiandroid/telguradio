package com.radios.india.models


import org.json.JSONArray

import java.util.ArrayList

/**
 * Created by Mohsin on 5/25/2018.
 */

class RadioDataModel {

    var catId: Int? = null
    var catName: String? = null
    var catList: JSONArray? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param catList
     * @param catName
     * @param catId
     */
    constructor(catId: Int?, catName: String, catList: JSONArray) : super() {
        this.catId = catId
        this.catName = catName
        this.catList = catList
    }

}
