package com.radios.india.models

import com.google.gson.annotations.SerializedName

import java.io.Serializable

/**
 * Created by Mohsin on 5/26/2018.
 */

class CatListModel : Serializable {


    @SerializedName("ch_id")
    var chId: Int? = null

    @SerializedName("ch_name")
    var chName: String? = null

    /*@SerializedName("ch_speed")
    private String chSpeed;*/

    /*public String getChSpeed() {
        return chSpeed;
    }

    public void setChSpeed(String chSpeed) {
        this.chSpeed = chSpeed;
    }*/

    @SerializedName("ch_url")
    var chUrl: String? = null

    @SerializedName("ch_url_2")
    var chUrl2: String? = null


    @SerializedName("ch_image")
    var chImage: String? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param chUrl
     * @param chSpeed
     * @param chId
     * @param chName
     * @param chImage
     */
    constructor(chId: Int?, chName: String, chSpeed: String, chUrl: String, chImage: String) : super() {
        this.chId = chId
        this.chName = chName
        //this.chSpeed = chSpeed;
        this.chUrl = chUrl
        this.chImage = chImage
    }
companion object{
    const val CH_URL ="ch_url"
    const val CH_URL_2 ="ch_url_2"

}
}
