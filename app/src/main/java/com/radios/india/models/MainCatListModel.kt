package com.radios.india.models

import java.util.ArrayList

/**
 * Created by Mohsin on 5/26/2018.
 */

class MainCatListModel {

    var catId: Int? = null
    var catName: String? = null
    private var catList: ArrayList<CatListModel>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param catList
     * @param catName
     * @param catId
     */
    constructor(catId: Int?, catName: String, catList: ArrayList<CatListModel>) : super() {
        this.catId = catId
        this.catName = catName
        this.catList = catList
    }

    fun getCatList(): List<CatListModel>? {
        return catList
    }

    fun setCatList(catList: ArrayList<CatListModel>) {
        this.catList = catList
    }

}
