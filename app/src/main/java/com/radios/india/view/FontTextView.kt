package com.radios.india.view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet

/**
 * Created by Mohsin on 5/23/2018.
 */

class FontTextView : androidx.appcompat.widget.AppCompatTextView {


    constructor(context: Context) : super(context) {
        setTypeFace(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setTypeFace(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setTypeFace(context)
    }

    private fun setTypeFace(context: Context) {
        val face = Typeface.createFromAsset(context.assets, "HighlandGothicLightFLF.ttf")
        this.typeface = face
    }
}
