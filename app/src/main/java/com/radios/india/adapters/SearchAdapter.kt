package com.radios.india.adapters


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.radios.india.R
import com.radios.india.RadioStationActivity
import com.radios.india.extra.StartServiceInterface
import com.radios.india.utils.Config
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class SearchAdapter(private val mContext: Context, private var itemsList: JSONArray?, private val startServiceInterface: StartServiceInterface) : RecyclerView.Adapter<SearchAdapter.SingleItemRowHolder>() {

    fun refresh(jsonArray: JSONArray) {
        itemsList = jsonArray
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.search_list_items, null)
        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        var singleItem: JSONObject?
        try {
            singleItem = itemsList!!.getJSONObject(i)
            holder.tv_song_name.text = singleItem!!.getString("ch_name")
            holder.tv_song_subtext.text = singleItem.getString("cat_name")
            //holder.tvRadioListeners.setText(singleItem.getString("ch_speed"));

            val transformation = RoundedTransformationBuilder()
                    .borderColor(Color.parseColor("#8c8c8c"))
                    .borderWidthDp(1f)
                    .cornerRadiusDp(10f)
                    .oval(false)
                    .build()

//           Picasso.Builder(holder.tv_song_name.context).build()
            Picasso.get()
                    .load(singleItem.getString("ch_image"))
                    .placeholder(R.drawable.fallback)
                    .error(R.drawable.fallback)
                    .fit()
                    .transform(transformation)
                    .into(holder.iv_song_image)

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, RadioStationActivity::class.java)
            Config.itemArray = this.itemsList!!
            Config.index = holder.adapterPosition
            startServiceInterface.startServiceFromMain()
            mContext.startActivity(intent)
            (mContext as Activity).overridePendingTransition(R.anim.slide_in_bottom_fade, R.anim.slide_out_top_fade)
        }

    }

    override fun getItemCount(): Int {
        return if (null != itemsList) itemsList!!.length() else 0
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_song_name: TextView = view.findViewById<View>(R.id.tv_song_name) as TextView
        var tv_song_subtext: TextView = view.findViewById<View>(R.id.tv_song_subtext) as TextView
        var iv_song_image: ImageView = view.findViewById<View>(R.id.iv_song_image) as ImageView

    }

}