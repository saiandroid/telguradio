package com.radios.india.adapters


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.radios.india.R
import com.radios.india.RadioStationActivity
import com.radios.india.extra.StartServiceInterface
import com.radios.india.utils.Config
import com.squareup.picasso.*

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception


class MainChildAdapter(private val mContext: Context, private val itemsList: JSONArray?, private val startServiceInterface: StartServiceInterface) : RecyclerView.Adapter<MainChildAdapter.SingleItemRowHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.main_list_single_card, null)
        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        var singleItem:JSONObject?
        try {
            singleItem = itemsList!!.getJSONObject(i)
            holder.tvTitle.text = singleItem!!.getString("ch_name")
            //holder.tvRadioListeners.setText(singleItem.getString("ch_speed"));

            val transformation = RoundedTransformationBuilder()
                    .borderColor(Color.parseColor("#8c8c8c"))
                    .borderWidthDp(1f)
                    .cornerRadiusDp(30f)
                    .oval(false)
                    .build()

//           Picasso.Builder(holder.tvTitle.context).build()
            Picasso.get()
                    .load(singleItem.getString("ch_image"))
                    .networkPolicy(NetworkPolicy.OFFLINE)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .placeholder(R.drawable.fallback)
                    .error(R.drawable.fallback)
                    .fit()
                    .transform(transformation)
                    .into(holder.itemImage, object:Callback {
                        override fun onError(e: Exception?) {

                            Picasso.get()
                                    .load(singleItem.getString("ch_image"))
//                                    .networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)

//                                    .memoryPolicy(MemoryPolicy.NO_CACHE,MemoryPolicy.NO_STORE)
                                    .placeholder(R.drawable.fallback)
                                    .error(R.drawable.fallback)
                                    .fit()
                                    .transform(transformation)
                                    .into(holder.itemImage)
                        }

                        override fun onSuccess() {
                        }


                    })


        } catch (e: JSONException) {
            e.printStackTrace()
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, RadioStationActivity::class.java)
            Config.itemArray = this.itemsList!!
            Config.index = holder.adapterPosition
            Config.fromMain = true
            startServiceInterface.startServiceFromMain()
            mContext.startActivity(intent)
            (mContext as Activity).overridePendingTransition(R.anim.slide_in_bottom_fade, R.anim.slide_out_top_fade)
        }
    }

    override fun getItemCount(): Int {
        return itemsList?.length() ?: 0
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvTitle: TextView = view.findViewById<View>(R.id.tvTitle) as TextView
        var itemImage: ImageView = view.findViewById<View>(R.id.itemImage) as ImageView

    }
}