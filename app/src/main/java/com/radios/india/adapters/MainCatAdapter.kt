package com.radios.india.adapters


import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.radios.india.R
import com.radios.india.ShowAllActivity
import com.radios.india.extra.StartServiceInterface
import com.radios.india.models.RadioDataModel
import com.radios.india.utils.Config
import com.radios.india.utils.MENU_TYPE

import org.json.JSONArray

import java.util.ArrayList

class MainCatAdapter(private val mContext: Context, private val dataList: ArrayList<RadioDataModel>?, private val startServiceInterface: StartServiceInterface) : RecyclerView.Adapter<MainCatAdapter.ItemRowHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.main_list_item, null)
        return ItemRowHolder(v)
    }

    override fun onBindViewHolder(itemRowHolder: ItemRowHolder, i: Int) {

        val sectionName = dataList!![i].catName
        val catList = dataList[i].catList

        itemRowHolder.itemTitle.text = sectionName

        val itemListDataAdapter = MainChildAdapter(mContext, catList, startServiceInterface)
      //  itemRowHolder.recycler_view_list.setHasFixedSize(true)
        itemRowHolder.recycler_view_list.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        itemRowHolder.recycler_view_list.adapter = itemListDataAdapter

        itemRowHolder.recycler_view_list.isNestedScrollingEnabled = false

        itemRowHolder.btnMore.setOnClickListener {
            val n = itemRowHolder.adapterPosition
            val intent = Intent(mContext, ShowAllActivity::class.java)

//            Config.itemArray = dataList[n].catList!!
//            Config.cat_name = dataList[n].catName!!

            Config.currentDisplayedArray = dataList[n].catList!!
            Config.currentDisplayedCatName = dataList[n].catName!!
            Config.menuType = MENU_TYPE.CAT
            mContext.startActivity(intent)
            (mContext as Activity).overridePendingTransition(R.anim.slide_in_bottom_fade, R.anim.slide_out_top_fade)
        }

    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    inner class ItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        var itemTitle: TextView = view.findViewById<View>(R.id.itemTitle) as TextView
        var recycler_view_list: RecyclerView = view.findViewById<View>(R.id.recycler_view_list) as RecyclerView
        var btnMore: TextView = view.findViewById<View>(R.id.btnMore) as TextView

    }

}