package com.radios.india.adapters


import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.radios.india.R
import com.radios.india.ShowAllActivity
import com.radios.india.extra.CloseDrawerInterface
import com.radios.india.models.RadioDataModel
import com.radios.india.utils.Config
import com.radios.india.utils.MENU_TYPE

import java.util.ArrayList

class GenresAdapter(private val mContext: Context, private val dataList: ArrayList<RadioDataModel>?, private val closeDrawerInterface: CloseDrawerInterface) : RecyclerView.Adapter<GenresAdapter.ItemRowHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.genres_list_items, null)
        return ItemRowHolder(v)
    }

    override fun onBindViewHolder(itemRowHolder: ItemRowHolder, i: Int) {

        val sectionName = dataList!![i].catName
        itemRowHolder.textView.text = sectionName

        itemRowHolder.itemView.setOnClickListener {
            val n = itemRowHolder.absoluteAdapterPosition
            val intent = Intent(mContext, ShowAllActivity::class.java)
//            Config.itemArray = dataList[n].catList!!
//            Config.cat_name = dataList[n].catName!!
            Config.currentDisplayedArray = dataList[n].catList!!
            Config.currentDisplayedCatName = dataList[n].catName!!
            Config.menuType = MENU_TYPE.CAT
            closeDrawerInterface.close()
            mContext.startActivity(intent)
            (mContext as Activity).overridePendingTransition(R.anim.slide_in_bottom_fade, R.anim.slide_out_top_fade)
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    inner class ItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textView: TextView = view.findViewById<View>(R.id.textView) as TextView
    }

}