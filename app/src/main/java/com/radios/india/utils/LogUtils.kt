package com.radios.india.utils

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.radios.india.BuildConfig

object LogUtils {
     fun logException(tag: String, message: String, throwable: Throwable?) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, message + "\nThrowable: " + throwable?.message)
            throwable?.printStackTrace()

        }else{
            if (throwable != null) {
                FirebaseCrashlytics.getInstance().recordException(throwable)
            }
        }
//        Crashlytics.log(Log.ERROR, tag, message + "\nThrowable: " + throwable?.message)
//        Crashlytics.logException(throwable)
//         Answers.getInstance().logCustom(CustomEvent(tag).putCustomAttribute(message,throwable.toString()))
// Set a key to a string.
    }
}