package com.radios.india.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import com.radios.india.R;

public class NotificationHelper {


    private NotificationManager notificationManager;

    private NotificationChannel notificationChannel;

    public NotificationHelper(Context context) {
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel existingNotificationChannel = notificationManager.getNotificationChannel(context.getString(R.string.default_notification_channel_id));
            if (existingNotificationChannel == null) {
                notificationChannel = new NotificationChannel(context.getString(R.string.default_notification_channel_id),
                        context.getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_LOW);
                notificationChannel.enableLights(false);
                notificationChannel.enableVibration(false);
                notificationChannel.setSound(null,null);


                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    public void notify(int notificationId, Notification notification) {
        try {
            notificationManager.notify(notificationId, notification);

        } catch (RuntimeException e) {
//            LogUtils.logException(TAG, "Error posting notification", e);
        }
    }

    public void cancel(int notificationId) {
        notificationManager.cancel(notificationId);
    }
}