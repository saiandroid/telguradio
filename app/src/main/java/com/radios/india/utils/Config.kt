package com.radios.india.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.core.content.ContextCompat.getSystemService
import com.radios.india.models.CatListModel
import com.radios.india.service.MediaPlayerService

import org.json.JSONArray

import java.util.ArrayList

/**
 * Created by Mohsin on 5/26/2018.
 */
enum class MENU_TYPE{
    FAV,
    ALL,
    CAT,
    SEARCH
}

object Config {
    var menuType:MENU_TYPE =MENU_TYPE.ALL
    var jsonArray = JSONArray()
    var itemArray = JSONArray()
        get() = field
        set(value) {
            field = value
        }
    var currentDisplayedArray = JSONArray() //for displaying the list of data like in see all, search results etc
        get() = field
        set(value) {
            field = value
        }
    open var index:Int  = 0
        get() = field
    set(value) {
        field = value
    }
    var launched  = false


    val Broadcast_PLAY_NEW_AUDIO = "com.valdioveliu.valdio.audioplayer.PlayNewAudio"
    var audioList: ArrayList<CatListModel>? = null
    set(value)  {
        field =value
    }
    get() = field
    var cat_name = ""

    var currentDisplayedCatName = "" //for displaying the list of data like in see all, search results etc
        get() = field
        set(value) {
            field = value
        }
    var shared_pref_name = "Radios_India"
    var shared_fav_list = "[]"
    var fromMain = false
        get() = field
        set(value) {
            field = value
        }
    var fromRadio = false
    var serviceStarted = false
    var exitedMain = false

   /* fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!.isConnected
    }*/

    @Suppress("DEPRECATION")
     fun isNetworkAvailable(activity: Activity): Boolean {
        if (activity.isFinishing) return false
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false
    }
}
