package com.radios.india

import android.annotation.SuppressLint
import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.google.firebase.analytics.FirebaseAnalytics


/**
 * Created by Mohsin on 6/1/2018.
 */

class MyApplication : MultiDexApplication() {

    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        super.onCreate()
        // CrashlyticsCore.getVersion()


//        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        Fabric.with(this, Crashlytics())

    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}
