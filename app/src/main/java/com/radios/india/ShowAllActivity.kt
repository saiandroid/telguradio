package com.radios.india

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.radios.india.adapters.ShowAllAdapter
import com.radios.india.databinding.ActivitySearchBinding
import com.radios.india.databinding.ActivityShowAllBinding
import com.radios.india.extra.StartServiceInterface
import com.radios.india.service.MediaPlayerService
import com.radios.india.utils.Config
import com.radios.india.utils.MENU_TYPE


class ShowAllActivity : AppCompatActivity(), StartServiceInterface {
    private lateinit var binding: ActivityShowAllBinding

    private var mAdView: AdView? = null
    private var mInterstitialAd: InterstitialAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShowAllBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //---------------------  Edge-to-Edge and System Bar Styling -------------------------//
        // Make the app go edge-to-edge
        WindowCompat.setDecorFitsSystemWindows(window, false)
        //Get the instance
        val windowInsetsController =
            WindowInsetsControllerCompat(window, window.decorView)
        windowInsetsController.isAppearanceLightStatusBars = false // Set to true if using a light background
        windowInsetsController.isAppearanceLightNavigationBars = false// Set to true if using a light background

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        enableEdgeToEdge()
        ViewCompat.setOnApplyWindowInsetsListener(binding.adView) { v, windowInsets ->
            val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
            // Apply the insets as a margin to the view. This solution sets
            // only the bottom, left, and right dimensions, but you can apply whichever
            // insets are appropriate to your layout. You can also update the view padding
            // if that's more appropriate.
            v.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                leftMargin = insets.left
                bottomMargin = insets.bottom
                rightMargin = insets.right
            }

            // Return CONSUMED if you don't want want the window insets to keep passing
            // down to descendant views.
            WindowInsetsCompat.CONSUMED
        }
        binding.customToolbarTitle.text = Config.currentDisplayedCatName
        if (Config.currentDisplayedArray.length() == 0) {
            binding.recyclerView.visibility = View.GONE
            if (Config.menuType == MENU_TYPE.FAV) {
                binding.noFav.visibility = View.VISIBLE
            }
            return
        }
//        recyclerView.setHasFixedSize(true)
//        val adapterAll = ShowAllAdapter(this@ShowAllActivity, Config.currentDisplayedArray, this)
//        recyclerView.layoutManager = GridLayoutManager(this@ShowAllActivity, 3)
//        recyclerView.adapter = adapterAll
        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView?.loadAd(adRequest)
        mAdView?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mAdView?.visibility = View.VISIBLE
            }

        }
    }

    override fun onResume() {
        super.onResume()
        if (Config.currentDisplayedArray.length() == 0) {
            binding.recyclerView.visibility = View.GONE
            if (Config.menuType == MENU_TYPE.FAV) {
                binding. noFav.visibility = View.VISIBLE
            }
            return
        }
        binding.recyclerView.setHasFixedSize(true)
        val adapterAll = ShowAllAdapter(this@ShowAllActivity, Config.currentDisplayedArray, this)
        binding.recyclerView.layoutManager = GridLayoutManager(this@ShowAllActivity, 3)
        binding.recyclerView.adapter = adapterAll
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun startServiceFromMain() {
        val playerIntent = Intent(this, MediaPlayerService::class.java)
        startService(playerIntent)
    }
}
