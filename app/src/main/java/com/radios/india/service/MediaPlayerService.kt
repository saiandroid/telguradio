package com.radios.india.service

import android.app.PendingIntent
import android.app.Service
import android.app.TaskStackBuilder
import android.bluetooth.BluetoothA2dp
import android.bluetooth.BluetoothHeadset
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ServiceInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.session.MediaSessionManager
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.RemoteException
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import androidx.core.content.ContextCompat
import com.devbrackets.android.exomedia.AudioPlayer
import com.devbrackets.android.exomedia.listener.OnErrorListener
import com.devbrackets.android.exomedia.listener.OnPreparedListener
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.radios.india.*
import com.radios.india.extra.PlaybackStatus
import com.radios.india.extra.RadioInterface
import com.radios.india.models.CatListModel
import com.radios.india.utils.Config
import com.radios.india.utils.LogUtils
import com.radios.india.utils.NotificationHelper
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.IOException


/**
 * Created by Valdio Veliu on 16-07-11.
 */
class MediaPlayerService : Service(), MediaPlayer.OnCompletionListener,
    MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnSeekCompleteListener,
    MediaPlayer.OnInfoListener,
    MediaPlayer.OnBufferingUpdateListener, AudioManager.OnAudioFocusChangeListener,
    OnErrorListener {

    private var mediaPlayer: AudioPlayer? = null

    //MediaSession
    private var mediaSessionManager: MediaSessionManager? = null
    private var mediaSession: MediaSessionCompat? = null
    private var transportControls: MediaControllerCompat.TransportControls? = null

    //Used to pause/resume MediaPlayer
    private val resumePosition: Int = 0

    private val TAG = "MediaPlayerService"


    //AudioFocus
    private var audioManager: AudioManager? = null

    // Binder given to clients
    private val iBinder = LocalBinder()

    //List of availableaudioList Audio files
    private var audioList: ArrayList<CatListModel>? = null
    private var audioIndex = -1
    private var activeAudio: CatListModel? = null //an object on the currently playing audio
    private var flag = false
    private var playingCh1 = true

    //Handle incoming phone calls
    private val ongoingCall = false

    val currentPlayingURL: String?
        get() = activeAudio?.chUrl

    val isPlaying: Boolean
        get() = mediaPlayer?.isPlaying ?: false


    /**
     * ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs
     */
    private val becomingNoisyReceiver = object : BroadcastReceiver() {
        override fun onReceive(
            context: Context,
            intent: Intent
        ) { //pause audio on ACTION_AUDIO_BECOMING_NOISY
            pauseMedia() //            buildNotification(PlaybackStatus.PAUSED)
        }
    }


    /**
     * Play new Audio
     */
    private val playNewAudio = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            //Get the new media index form SharedPreferences
            audioIndex = Config.index
            if (audioIndex != -1 && audioIndex < audioList!!.size) { //index is in a valid range
                activeAudio = audioList!![audioIndex]
            } else {
                stopSelf()
            }

            //A PLAY_NEW_AUDIO action received
            //reset mediaPlayer to play the new Audio
            stopMedia()
            mediaPlayer!!.reset()
            initMediaPlayer()
            updateMetaData()
            buildNotification(PlaybackStatus.PLAYING)
        }
    } //private PhoneStateListener phoneStateListener; //private TelephonyManager telephonyManager;


    /**
     * Service lifecycle methods
     */
    override fun onBind(intent: Intent): IBinder? {
        return iBinder
    }

    override fun onCreate() {
        super.onCreate()

        notificationHelper = NotificationHelper(this) // Perform one-time setup procedures

        // Manage incoming phone calls during playback.
        // Pause MediaPlayer on incoming call,
        // Resume on hangup.
        //callStateListener();
        //ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs -- BroadcastReceiver
        try { //Load data from SharedPreferences
            //StorageUtil storage = new StorageUtil(getApplicationContext());
            Config.audioList = getAudioList()

            audioList = Config.audioList
            audioIndex = Config.index

            if (audioIndex != -1 && audioIndex < audioList!!.size) { //index is in a valid range
                activeAudio = audioList!![audioIndex]
            } else {
                stopSelf()
            }
        } catch (e: NullPointerException) {
            stopSelf()
        }

        registerBecomingNoisyReceiver() //Listen for new Audio to play -- BroadcastReceiver
        registerPlayNewAudio()
    }

    //The system calls this method when an activity, requests the service be started
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        try { //Request audio focus
            if (!requestAudioFocus()) { //Could not gain focus
                stopSelf()
            }

            if (mediaSession == null) {
                try {
                    initMediaSession()
                    initMediaPlayer()
                } catch (e: RemoteException) {
                    e.printStackTrace()
                    stopSelf()
                }

                buildNotification(PlaybackStatus.PLAYING)
            }

            //Handle Intent action from MediaSession.TransportControls
            handleIncomingActions(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return START_STICKY
    }

    private fun getAudioList(): ArrayList<CatListModel>? {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<CatListModel>>() {

        }.type
        return gson.fromJson<ArrayList<CatListModel>>(Config.itemArray.toString(), type)
    }

    override fun onUnbind(intent: Intent): Boolean {
        try {
            mediaSession!!.release() //removeNotification()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaPlayer != null) {
            stopMedia()
            mediaPlayer!!.release()
        }
        try {
            removeAudioFocus()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        //Disable the PhoneStateListener
        /*if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }*/

        removeNotification()

        //unregister BroadcastReceivers
        unregisterReceiver(becomingNoisyReceiver)
        unregisterReceiver(playNewAudio)

        //clear cached playlist
        //new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();
    }

    /**
     * Service Binder
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        val service: MediaPlayerService
            get() = this@MediaPlayerService
    }

    /**
     * MediaPlayer callback methods
     */
    override fun onBufferingUpdate(
        mp: MediaPlayer,
        percent: Int
    ) { //Invoked indicating buffering status of
        //a media resource being streamed over the network.
        radioInterface!!.buffering()
    }

    override fun onCompletion(mp: MediaPlayer) { //Invoked when playback of a media source has completed.
        stopMedia()

        removeNotification() //stop the service
        stopSelf()
    }

    override fun onError(
        mp: MediaPlayer,
        what: Int,
        extra: Int
    ): Boolean { //Invoked when there has been an error during an asynchronous operation
        when (what) {
            MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK -> Log.d(
                "MediaPlayer Error",
                "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra
            )

            MediaPlayer.MEDIA_ERROR_SERVER_DIED -> Log.d(
                "MediaPlayer Error",
                "MEDIA ERROR SERVER DIED " + extra
            )

            MediaPlayer.MEDIA_ERROR_UNKNOWN -> Log.d(
                "MediaPlayer Error",
                "MEDIA ERROR UNKNOWN " + extra
            )
        }
        return false
    }

    override fun onInfo(
        mp: MediaPlayer,
        what: Int,
        extra: Int
    ): Boolean { //Invoked to communicate some info
        return false
    }

    override fun onPrepared(mp: MediaPlayer) { //Invoked when the media source is ready for playback.
        playMedia() //        radioInterface!!.stopBuffering()
        buildNotification(PlaybackStatus.PLAYING)
    }

    override fun onSeekComplete(mp: MediaPlayer) { //Invoked indicating the completion of a seek operation.
    }

    override fun onAudioFocusChange(focusState: Int) {

        //Invoked when the audio focus of the system is updated.
        when (focusState) {
            AudioManager.AUDIOFOCUS_GAIN -> { // resume playback
                if (mediaPlayer == null && flag) {
                    initMediaPlayer()
                    flag = false
                } else if (!mediaPlayer!!.isPlaying && flag) {
                    mediaPlayer?.start()
                    flag = false
                }

                // mediaPlayer!!.setVolume(1.0f, 1.0f)
            }

            AudioManager.AUDIOFOCUS_LOSS -> { // Lost focus for an unbounded amount of time: stop playback and release media player
                /*if (mediaPlayer!!.isPlaying) mediaPlayer!!.stopPlayback()
                mediaPlayer!!.release()
                mediaPlayer = null*/
            }

            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.pause()
                    flag = true
                }

            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {

            } // Lost focus for a short time, but it's ok to keep playing
            // at an attenuated level
            //if (mediaPlayer!!.isPlaying) mediaPlayer!!.setVolume(0.1f, 0.1f)
        }
    }


    private fun requestAudioFocus(): Boolean {
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val result = audioManager!!.requestAudioFocus(
            this,
            AudioManager.STREAM_MUSIC,
            AudioManager.AUDIOFOCUS_GAIN
        )
        return result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED //Could not gain focus
    }

    private fun removeAudioFocus(): Boolean {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == audioManager!!.abandonAudioFocus(this)
    }

    var stationOffline = false

    override fun onError(e: java.lang.Exception?): Boolean {
        radioInterface?.error()
        if (e?.cause is HttpDataSource.HttpDataSourceException) { //Offline
            stationOffline = true
            if (playingCh1) {
                playingCh1 = false //trying to play 2nd one
                initMediaPlayer()
            } else {
                playingCh1 = true
            }
        }
        e?.printStackTrace()
        return false
    }

    /**
     * MediaPlayer actions
     */
    private fun initMediaPlayer() {
        try {
            if (mediaPlayer == null) {
                mediaPlayer = AudioPlayer(applicationContext) //new MediaPlayer instance
            }
            mediaPlayer?.let { mediaPlayer ->
                mediaPlayer.setOnPreparedListener(object : OnPreparedListener {
                    override fun onPrepared() {
                        mediaPlayer.start()
                        buildNotification(PlaybackStatus.PLAYING)
                        radioInterface?.stopBuffering()
                    }

                })
                mediaPlayer.setOnErrorListener(this) //Reset so that the MediaPlayer is not pointing to another data source
                mediaPlayer.reset()
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC) // Set the data source to the mediaFile location
                if (activeAudio == null) return
                val channelUrl = if (playingCh1) {
                    activeAudio?.chUrl
                } else {
                    activeAudio?.chUrl2
                }
                val streamType = if (channelUrl?.endsWith(".m3u8") == true) "dash" else null
                val mediaSource = buildMediaSource(Uri.parse(channelUrl), streamType)
                mediaPlayer.setMedia(Uri.parse(channelUrl), mediaSource)
                mediaPlayer.start()
            }
        } catch (e: Exception) {
            LogUtils.logException(TAG, "initMedia error", e)
        }
    }

    private val dataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(this, "TelguRadioOnline")
    }
    private val customPolicy = CustomPolicy()
    private fun buildMediaSource1(uri: Uri): MediaSource {

        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .setLoadErrorHandlingPolicy(customPolicy).createMediaSource(MediaItem.fromUri(uri))

    }

    private fun buildMediaSource(uri: Uri, type: String?): MediaSource {
        return if (type == "dash") {
            HlsMediaSource.Factory(dataSourceFactory).createMediaSource(MediaItem.fromUri(uri))
        } else {
            ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(MediaItem.fromUri(uri))
        }
    }

    internal class CustomPolicy : DefaultLoadErrorHandlingPolicy() {
        fun getRetryDelayMsFor(
            dataType: Int,
            loadDurationMs: Long,
            exception: IOException?,
            errorCount: Int
        ): Long { // Replace NoConnectivityException with the corresponding
            // exception for the used DataSource.
            return if (exception is java.lang.Exception) {
                15000 // Retry every 5 seconds.
            } else {
                C.TIME_UNSET // Anything else is surfaced.
            }
        }

        override fun getMinimumLoadableRetryCount(dataType: Int): Int {
            return Int.MAX_VALUE
        }
    }

    private fun playMedia() {
        try {
            if (!mediaPlayer!!.isPlaying) {
                mediaPlayer!!.start()
            }
        } catch (e: Exception) {
            LogUtils.logException(TAG, "playMedia error", e)
        }
    }

    private fun stopMedia() {
        try {
            if (mediaPlayer == null) return
            if (mediaPlayer!!.isPlaying) {
                mediaPlayer?.stop();
                mediaPlayer?.seekTo(0);
            }
        } catch (e: java.lang.Exception) {
            LogUtils.logException(TAG, "stopMedia error", e)
            mediaPlayer = null
            initMediaPlayer()
        }
    }

    private fun pauseMedia() {
        try {
            if (mediaPlayer!!.isPlaying) {
                mediaPlayer!!.pause()
                buildNotification(PlaybackStatus.PAUSED) //resumePosition = mediaPlayer.getCurrentPosition();
            }
        } catch (e: Exception) {
            LogUtils.logException(TAG, "pauseMedia error", e)

            mediaPlayer = null
            initMediaPlayer()
        }
    }

    private fun resumeMedia() {
        try {
            if (!mediaPlayer!!.isPlaying) { //mediaPlayer.seekTo(resumePosition);
                mediaPlayer!!.start()
                buildNotification(PlaybackStatus.PLAYING)

            }
        } catch (e: java.lang.Exception) {
            LogUtils.logException(TAG, "resumeMedia error", e)


            mediaPlayer = null
            initMediaPlayer();
        }
    }

    fun skipToNext() {

        try {

            if (BuildConfig.DEBUG) {
                if (audioList == null) {
                    throw RuntimeException(" audioList is null ")
                }

                if (audioList!!.size <= 0) {
                    throw RuntimeException("skipToNext audioList is zero ")
                }
            }

            if (audioIndex == audioList!!.size - 1) { //if last in playlist
                audioIndex = 0
                activeAudio = audioList!![audioIndex]
            } else { //get next in playlist
                activeAudio = audioList!![++audioIndex]
            }

            //Update stored index
            //new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);
            Config.index = audioIndex

            stopMedia() //reset mediaPlayer
            mediaPlayer!!.reset()
            initMediaPlayer()
        } catch (e: Exception) {
            LogUtils.logException(TAG, "skipTonext error", e)
        }
    }

    private fun skipToPrevious() {


        try {
            if (BuildConfig.DEBUG) {
                if (audioList == null) {
                    throw RuntimeException(" audioList is null ")
                }

                if (audioList!!.size <= 0) {
                    throw RuntimeException("skipToNext audioList is zero ")
                }
            }
            if (audioIndex == 0) { //if first in playlist
                //set index to the last of audioList
                audioIndex = audioList!!.size - 1
                activeAudio = audioList!![audioIndex]
            } else { //get previous in playlist
                activeAudio = audioList!![--audioIndex]
            }

            //Update stored index
            //new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);
            Config.index = audioIndex

            stopMedia() //reset mediaPlayer
            mediaPlayer!!.reset()
            initMediaPlayer()
        } catch (e: Exception) {
            LogUtils.logException(TAG, "skipToPrevious error", e)
        }
    }

    fun onPlay() {
        resumeMedia()
    }

    fun onPause() {
        pauseMedia()
    }

    private var fromUserClick = false;

    fun playNewAudio() {
        try {

            audioList = Config.audioList
            audioIndex = Config.index
            playingCh1 = true
            if (audioIndex != -1 && audioIndex < audioList!!.size) { //index is in a valid range
                activeAudio = audioList!![audioIndex]
                stopMedia() //reset mediaPlayer
                mediaPlayer!!.reset()
                radioInterface!!.buffering()
                fromUserClick = true
                initMediaPlayer()
                updateMetaData()
                buildNotification(PlaybackStatus.PLAYING)
            } else {
                stopSelf()
            }
        } catch (e: Exception) {
            LogUtils.logException(TAG, "playNewAudio error", e)
            stopSelf()
        }

    }

    fun onStop() {
        stopMedia()
        removeNotification() //radioInterface!!.stop()
    }

    fun updateData() {
        try { //Load data from SharedPreferences
            //StorageUtil storage = new StorageUtil(getApplicationContext());
            Config.audioList = getAudioList()
            audioList = Config.audioList
            audioIndex = Config.index

            if (audioIndex != -1 && audioIndex < audioList!!.size) { //index is in a valid range
                activeAudio = audioList!![audioIndex]
            } else {
                stopSelf()
            }
        } catch (e: NullPointerException) {
            stopSelf()
        }
    }

    fun onSkipToNext() {
        playingCh1 = true
        radioInterface!!.buffering()
        skipToNext()
        fromUserClick = true
        updateMetaData()
        buildNotification(PlaybackStatus.PLAYING)
    }

    fun onSkipToPrevious() {
        playingCh1 = true
        radioInterface?.buffering()
        skipToPrevious()
        fromUserClick = true
        updateMetaData()
        buildNotification(PlaybackStatus.PLAYING)
    }

    private fun registerBecomingNoisyReceiver() { //register after getting audio focus
        val intentFilter = IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(becomingNoisyReceiver, intentFilter, RECEIVER_EXPORTED)
        }else{
            registerReceiver(becomingNoisyReceiver, intentFilter)
        }
    }

    /**
     * Handle PhoneState changes
     *//*private void callStateListener() {
        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    //if at least one call exists or the phone is ringing
                    //pause the MediaPlayer
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (mediaPlayer != null) {
                            pauseMedia();
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        // Phone idle. Start playing.
                        if (mediaPlayer != null) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                resumeMedia();
                            }
                        }
                        break;
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }*/

    /**
     * MediaSession and Notification actions
     */
    @Throws(RemoteException::class)
    private fun initMediaSession() {
        if (mediaSession != null) return  //mediaSessionManager exists

        if (Build.VERSION.SDK_INT >= 21) {
            mediaSessionManager =
                getSystemService(Context.MEDIA_SESSION_SERVICE) as MediaSessionManager
        } // Create a new MediaSession
        mediaSession = MediaSessionCompat(applicationContext, "AudioPlayer")
        mediaSession?.let { mediaSession ->


            //Get MediaSessions transport controls
            transportControls =
                mediaSession.controller.transportControls //set MediaSession -> ready to receive media commands
            mediaSession.isActive =
                true //indicate that the MediaSession handles transport control commands
            // through its MediaSessionCompat.Callback.
            mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)

            //Set mediaSession's MetaData
            updateMetaData()

            // Attach Callback to receive MediaSession updates
            mediaSession.setCallback(object : MediaSessionCompat.Callback() {
                // Implement callbacks
                override fun onPlay() {
                    super.onPlay()

                    resumeMedia() //                buildNotification(PlaybackStatus.PLAYING)
                }

                override fun onPause() {
                    super.onPause()

                    pauseMedia() //                buildNotification(PlaybackStatus.PAUSED)
                }

                override fun onSkipToNext() {
                    super.onSkipToNext()
                    radioInterface!!.buffering()
                    skipToNext()
                    updateMetaData()
                    buildNotification(PlaybackStatus.PLAYING)
                }

                override fun onSkipToPrevious() {
                    super.onSkipToPrevious()
                    radioInterface!!.buffering()
                    skipToPrevious()
                    updateMetaData()
                    buildNotification(PlaybackStatus.PLAYING)
                }

                override fun onStop() {
                    super.onStop()
                    removeNotification() //Stop the service
                    stopSelf()
                }

                override fun onPrepare() {
                    super.onPrepare() //                    radioInterface!!.stopBuffering()
                    buildNotification(PlaybackStatus.PLAYING)
                }

            })
        }
    }

    private fun updateMetaData() {
        if (activeAudio == null) {
            return
        }
        val albumArt = BitmapFactory.decodeResource(
            resources,
            R.drawable.category_bg
        ) //replace with medias albumArt // Update the current metadata
        mediaSession!!.setMetadata(
            MediaMetadataCompat.Builder()
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, albumArt)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, activeAudio!!.chName)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, "")
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, getString(R.string.app_name))
                .build()
        )
    }

    private fun buildNotification(playbackStatus: PlaybackStatus) {
        try {

            /**
             * Notification actions -> playbackAction()
             * 0 -> Play
             * 1 -> Pause
             * 2 -> Next track
             * 3 -> Previous track
             */
            val stackBuilder = TaskStackBuilder.create(this)
            val intent = Intent(this, RadioStationActivity::class.java)
            intent.putExtra("fromService", true)
            val mainIntent = Intent(this, MainActivity::class.java)
            stackBuilder.addNextIntent(mainIntent)
            stackBuilder.addNextIntent(intent) //        stackBuilder.addNextIntentWithParentStack(intent)
            //largeIcon= BitmapFactory.decodeResource(getResources(), R.drawable.category_bg);
            //        stackBuilder.addNextIntentWithParentStack(intent)

            //        Picasso.get().load(activeAudio!!.chImage).placeholder(R.drawable.rescueimg6)
            Picasso.get() //        Picasso.Builder(applicationContext).build()
                .load(activeAudio!!.chImage).placeholder(R.drawable.rescueimg6)

                .error(R.drawable.rescueimg6).into(object : Target {
                    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {

                        val contentIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_IMMUTABLE)
                        } else {
                            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                        }


                        var notificationAction =
                            android.R.drawable.ic_media_pause //needs to be initialized
                        var playPauseAction: PendingIntent? = null

                        //Build a new notification according to the current state of the MediaPlayer
                        if (playbackStatus === PlaybackStatus.PLAYING) {
                            notificationAction =
                                android.R.drawable.ic_media_pause //create the pause action
                            playPauseAction = playbackAction(1)
                        } else if (playbackStatus === PlaybackStatus.PAUSED) {
                            notificationAction =
                                android.R.drawable.ic_media_play //create the play action
                            playPauseAction = playbackAction(0)
                        }

                        val notificationBuilder = androidx.core.app.NotificationCompat.Builder(
                            applicationContext,
                            getString(R.string.default_notification_channel_id)
                        ) // Hide the timestamp
                            .setShowWhen(false).setStyle(
                                androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1, 2)
                            ).setAutoCancel(false).setColor(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccent
                                )
                            ) // Set the large and small icons
                            .setLargeIcon(bitmap).setSmallIcon(R.drawable.ic_launcher_ticker_2)
                            .setContentIntent(contentIntent).setContentTitle(activeAudio!!.chName)
                            .setChannelId(getString(R.string.default_notification_channel_id))
                            .addAction(
                                android.R.drawable.ic_media_previous,
                                "previous",
                                playbackAction(3)
                            ).addAction(notificationAction, "pause", playPauseAction)
                            .addAction(android.R.drawable.ic_media_next, "next", playbackAction(2))
                        val notification =
                            notificationBuilder.build() //  notification.flags = notification.flags or Notification.FLAG_NO_CLEAR
                        notificationHelper?.notify(NOTIFICATION_ID, notification)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            try {
                                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
                                    startForeground(NOTIFICATION_ID, notification)
                                } else {
                                    startForeground(
                                        NOTIFICATION_ID,
                                        notification,
                                        ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
                                    )

                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                    }

                    override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {

                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable) {

                    }
                })
        } catch (e: Exception) {
            LogUtils.logException(TAG, "buildNotification error", e)
            val intent = Intent(this, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        /*Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.category_bg);*/ //replace with your own image

        // Create a new Notification
    }


    private fun removeNotification() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                stopForeground(true)
            } else {
                stopForeground(false)
            }
            notificationHelper?.cancel(NOTIFICATION_ID)
        } catch (e: Exception) {
            LogUtils.logException(TAG, "removeNotification error", e)
        }/*val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)*/
    }

    private fun handleIncomingActions(playbackAction: Intent?) {
        if (playbackAction == null || playbackAction.action == null) return

        val actionString = playbackAction.action
        when {
            actionString.equals(ACTION_PLAY, ignoreCase = true) -> { //transportControls!!.play()
                resumeMedia()
            }

            actionString.equals(ACTION_PAUSE, ignoreCase = true) -> { // transportControls!!.pause()
                pauseMedia()
            }

            actionString.equals(
                ACTION_NEXT,
                ignoreCase = true
            ) -> { //                transportControls!!.skipToNext()
                skipToNext()

            }

            actionString.equals(
                ACTION_PREVIOUS,
                ignoreCase = true
            ) -> { //                transportControls!!.skipToPrevious()
                skipToPrevious()
            }

            actionString.equals(ACTION_STOP, ignoreCase = true) -> {
                stopMedia()
            }
        }
    }

    private fun registerPlayNewAudio() { //Register playNewMedia receiver
        val filter = IntentFilter(Config.Broadcast_PLAY_NEW_AUDIO)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(playNewAudio, filter, RECEIVER_NOT_EXPORTED)
        }else{
            registerReceiver(playNewAudio, filter)
        }
    }


    companion object {


        val ACTION_PLAY = "com.valdioveliu.valdio.audioplayer.ACTION_PLAY"
        val ACTION_PAUSE = "com.valdioveliu.valdio.audioplayer.ACTION_PAUSE"
        val ACTION_PREVIOUS = "com.valdioveliu.valdio.audioplayer.ACTION_PREVIOUS"
        val ACTION_NEXT = "com.valdioveliu.valdio.audioplayer.ACTION_NEXT"
        val ACTION_STOP = "com.valdioveliu.valdio.audioplayer.ACTION_STOP"
        private var radioInterface: RadioInterface? = null

        //AudioPlayer notification ID
        private val NOTIFICATION_ID = 101

        fun setInterface(radioInterface1: RadioInterface) {
            radioInterface = radioInterface1
        }
    }

    private fun playbackAction(actionNumber: Int): PendingIntent? {
        val playbackAction = Intent(this, MediaPlayerService::class.java)
        val flagValue =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else 0
        when (actionNumber) {
            0 -> { // Play
                radioInterface!!.play()
                playbackAction.action = ACTION_PLAY
            }

            1 -> { // Pause
                radioInterface?.pause()
                playbackAction.action = ACTION_PAUSE
            }

            2 -> { // Next track
                radioInterface?.next()
                playbackAction.action = ACTION_NEXT
            }

            3 -> { // Previous track
                radioInterface?.prev()
                playbackAction.action = ACTION_PREVIOUS
            }

        }
        return PendingIntent.getService(this, actionNumber, playbackAction, flagValue)
    }


    internal var notificationHelper: NotificationHelper? = null


    //bluetooth

    public class MediaButtonCommand {

        public companion object {
            @kotlin.jvm.JvmField
            var CMD_NAME: String = "command"
            val APPLE_SIZE_KEY: String = "APPLE_SIZE_KEY"

            //            public val CMD_NAME:String= "command"
            @kotlin.jvm.JvmField
            public val TOGGLE_PAUSE = "togglepause"

            @kotlin.jvm.JvmField
            public val STOP = "stop"

            @kotlin.jvm.JvmField
            public val PAUSE = "pause"

            @kotlin.jvm.JvmField
            public val PLAY = "play"

            @kotlin.jvm.JvmField
            public val PREVIOUS = "previous"

            @kotlin.jvm.JvmField
            public val NEXT = "next"

            @kotlin.jvm.JvmField
            public val TOGGLE_FAVORITE = "togglefavorite"

            @kotlin.jvm.JvmField
            public val FROM_MEDIA_BUTTON = "frommediabutton"
        }


    }

    private var bluetoothReceiver: BroadcastReceiver? = null
    private var a2dpReceiver: BroadcastReceiver? = null

    private var bluetoothReceiverIsRegistered: Boolean = false

    private fun registerBluetoothReceiver() {
        val filter = IntentFilter()
        filter.addAction(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED)
        filter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED)

        bluetoothReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                val action = intent.action
                if (action != null) {
                    val extras =
                        intent.extras //                    if (SettingsManager.getInstance().getBluetoothPauseDisconnect()) {
                    when (action) {
                        BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED -> if (extras != null) {
                            val state = extras.getInt(BluetoothA2dp.EXTRA_STATE)
                            val previousState = extras.getInt(BluetoothA2dp.EXTRA_PREVIOUS_STATE)
                            if ((state == BluetoothA2dp.STATE_DISCONNECTED || state == BluetoothA2dp.STATE_DISCONNECTING) && previousState == BluetoothA2dp.STATE_CONNECTED) {
                                pauseMedia()
                            }
                        }

                        BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED -> if (extras != null) {
                            val state = extras.getInt(BluetoothHeadset.EXTRA_STATE)
                            val previousState = extras.getInt(BluetoothHeadset.EXTRA_PREVIOUS_STATE)
                            if (state == BluetoothHeadset.STATE_AUDIO_DISCONNECTED && previousState == BluetoothHeadset.STATE_AUDIO_CONNECTED) {
                                pauseMedia()
                            }
                        }
                    } //                    }

                    //                    if (SettingsManager.getInstance().getBluetoothResumeConnect()) {
                    when (action) {
                        BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED -> if (extras != null) {
                            val state = extras.getInt(BluetoothA2dp.EXTRA_STATE)
                            if (state == BluetoothA2dp.STATE_CONNECTED) {
                                playMedia()
                            }
                        }

                        BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED -> if (extras != null) {
                            val state = extras.getInt(BluetoothHeadset.EXTRA_STATE)
                            if (state == BluetoothHeadset.STATE_AUDIO_CONNECTED) {
                                playMedia()
                            }
                        }
                    } //                    }
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(bluetoothReceiver, filter, RECEIVER_EXPORTED)
        }else{
            registerReceiver(bluetoothReceiver, filter)
        }
        bluetoothReceiverIsRegistered = true
    }

    private fun unregisterBluetoothReceiver() {
        if (bluetoothReceiverIsRegistered) {
            unregisterReceiver(bluetoothReceiver)
            bluetoothReceiverIsRegistered = false
        }
    }

}
