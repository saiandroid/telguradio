package com.radios.india


import android.Manifest.permission.POST_NOTIFICATIONS
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.google.android.material.navigation.NavigationView
import com.google.android.ump.ConsentDebugSettings
import com.google.android.ump.ConsentDebugSettings.DebugGeography.DEBUG_GEOGRAPHY_EEA
import com.google.android.ump.ConsentInformation
import com.google.android.ump.ConsentRequestParameters
import com.google.android.ump.UserMessagingPlatform
import com.google.gson.Gson
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.radios.india.adapters.GenresAdapter
import com.radios.india.adapters.MainCatAdapter
import com.radios.india.databinding.ActivityMainBinding
import com.radios.india.extra.CloseDrawerInterface
import com.radios.india.extra.RadioInterface
import com.radios.india.extra.StartServiceInterface
import com.radios.india.models.CatListModel
import com.radios.india.models.RadioDataModel
import com.radios.india.service.MediaPlayerService
import com.radios.india.utils.Config
import com.radios.india.utils.LogUtils
import com.radios.india.utils.MENU_TYPE
import com.squareup.picasso.Picasso
import org.json.JSONArray


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    CloseDrawerInterface, RadioInterface, StartServiceInterface {
    private lateinit var binding: ActivityMainBinding

    var radioDataModel: ArrayList<RadioDataModel> = ArrayList();
    var i: Intent = Intent()
    var flag_genres = false
    var flag_exit = false
    private val TAG = "MediaPlayerService"
    private lateinit var consentInformation: ConsentInformation

    // Show a privacy options button if required.
    private val isPrivacyOptionsRequired: Boolean
        get() =
            consentInformation.privacyOptionsRequirementStatus ==
                    ConsentInformation.PrivacyOptionsRequirementStatus.REQUIRED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //---------------------  Edge-to-Edge and System Bar Styling -------------------------//
        // Make the app go edge-to-edge
        WindowCompat.setDecorFitsSystemWindows(window, false)
        //Get the instance
        val windowInsetsController =
            WindowInsetsControllerCompat(window, window.decorView)
        // Configure the status bar and navigation bar
        windowInsetsController.isAppearanceLightStatusBars = false // Set to true if using a light background
        windowInsetsController.isAppearanceLightNavigationBars = false// Set to true if using a light background

        //---------------------------- End of edge to edge --------------------------------------//

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.appBarMain.toolbar)
        Config.exitedMain = false
        val debugSettings = ConsentDebugSettings.Builder(this)
            .setDebugGeography(DEBUG_GEOGRAPHY_EEA)
            .addTestDeviceHashedId("5660A47A6E7B28F1ABCFF49FA95C61C4")
            .addTestDeviceHashedId("C0B31AE74793DCC354E50098BE39F4CD")
            .build()

        // Create a ConsentRequestParameters object.
        val builder = ConsentRequestParameters
            .Builder()
            .setConsentDebugSettings(debugSettings)

        if(BuildConfig.DEBUG){
            builder.setConsentDebugSettings(debugSettings)
        }
        val params = builder.build()
        consentInformation = UserMessagingPlatform.getConsentInformation(this)
        consentInformation.requestConsentInfoUpdate(
            this,
            params,
            {
                UserMessagingPlatform.loadAndShowConsentFormIfRequired(
                    this@MainActivity
                ) { loadAndShowError ->
                    // Consent gathering failed.
                    if (loadAndShowError != null) {
                        Log.w(
                            TAG, String.format(
                                "%s: %s",
                                loadAndShowError.errorCode,
                                loadAndShowError.message
                            )
                        )
                    }

                    // Consent has been gathered.
                    if (isPrivacyOptionsRequired) {
                        // Regenerate the options menu to include a privacy setting.
                        invalidateOptionsMenu();
                    }
                    if (consentInformation.canRequestAds()) {
                        initializeMobileAdsSdk()
                    }
                }
            },
            { requestConsentError ->
                // Consent gathering failed.
                Log.w(
                    TAG, String.format(
                        "%s: %s",
                        requestConsentError.errorCode,
                        requestConsentError.message
                    )
                )
            })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(this, arrayOf(POST_NOTIFICATIONS), 101);
            }
        }
//        LogUtils.logException(TAG,"onCreate error test",Exception("onCreate"))


        /*fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/

        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.appBarMain.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        binding.navView.setNavigationItemSelectedListener(this)
        binding.sideNavFooter.rlRateUs.setOnClickListener {
            try {
                i = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.market_app_link)))
                startActivity(i)
            } catch (error: ActivityNotFoundException) {
                i = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.google_app_link)))
                startActivity(i)
            }

            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }
        binding.sideNavFooter.rlGdprPrivacy.setOnClickListener {
            UserMessagingPlatform.showPrivacyOptionsForm(this) { formError ->
                formError?.let {
                    // Handle the error.
                    Log.e("Main: ", it.message)
                }
            }
        }
        binding.navView.setNavigationItemSelectedListener(this)
        binding.sideNavFooter.rlRateUs1.setOnClickListener {
            try {
                i = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.market_app_link1)))
                startActivity(i)
            } catch (error: ActivityNotFoundException) {
                i = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.google_app_link1)))
                startActivity(i)
            }

            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }

        binding.sideNavFooter.rlShare.setOnClickListener {
            i = Intent("android.intent.action.SEND")
            i.type = "text/plain"
            i.putExtra("android.intent.extra.TEXT", getString(R.string.share_text))
            startActivity(Intent.createChooser(i, getString(R.string.share_via)))

            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }

        binding.sideNavFooter.rlSubmitFeedback.setOnClickListener {
            i = Intent(Intent.ACTION_SEND)
            i.type = "message/rfc822"
            i.putExtra(Intent.EXTRA_SUBJECT, "Telugu Radios V 3.4 Feedback")
            i.putExtra(Intent.EXTRA_EMAIL, arrayOf("radiosmalaysia@gmail.com"))
            val mailer = Intent.createChooser(i, "Send Feedback via:")
            startActivity(mailer)

            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }

        binding.navView.findViewById<RelativeLayout>(R.id.rl_home).setOnClickListener {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }

        binding.sideNavFooter.rlFav.setOnClickListener {
            var jsonArray = JSONArray();
            val sp = getSharedPreferences(Config.shared_pref_name, Activity.MODE_PRIVATE)
            val str = sp.getString(Config.shared_fav_list, jsonArray.toString())
            jsonArray = JSONArray(str)
//            Config.itemArray=jsonArray
//            Config.cat_name=getString(R.string.favourites)

            Config.currentDisplayedArray = jsonArray
            Config.currentDisplayedCatName = getString(R.string.favourites)
            Config.menuType = MENU_TYPE.FAV

            i = Intent(this@MainActivity, ShowAllActivity::class.java)
            startActivity(i)
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }

        binding.rlDisclaimer.setOnClickListener {
            i = Intent(this@MainActivity, WebviewActivity::class.java)
            i.putExtra("page", "disc")
            startActivity(i)
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }

        binding.sideNavFooter.rlHelp.setOnClickListener() {
            i = Intent(this@MainActivity, WebviewActivity::class.java)
            i.putExtra("page", "help")
            startActivity(i)
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }

        binding.sideNavFooter.rlPrivacy.setOnClickListener {
            i = Intent(this@MainActivity, WebviewActivity::class.java)
            i.putExtra("page", "privacy")
            startActivity(i)
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }




        try {
            if (Config.itemArray.length() > 0) {
                playAudio()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        binding.navView.findViewById<RelativeLayout>(R.id.rl_exit).setOnClickListener() {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            try {
                if (!serviceBound) {
                    flag_exit = true
                    playAudio()
                } else {
                    player?.stopSelf()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            finish()

        }

        binding.sideNavFooter.navRecyclerView.isNestedScrollingEnabled = false
        binding.navView.findViewById<RelativeLayout>(R.id.rl_genres).setOnClickListener() {
            val indicator = binding.navView.findViewById<ImageView>(R.id.indicator)
            if (!flag_genres) {
                flag_genres = true
                binding.sideNavFooter.navRecyclerView.visibility = View.VISIBLE
                indicator.setImageResource(R.drawable.ic_indicator_expanded)
            } else {
                flag_genres = false
                binding.sideNavFooter.navRecyclerView.visibility = View.GONE
                indicator.setImageResource(R.drawable.ic_indicator)
            }
        }

        binding.appBarMain.search.setOnClickListener() {
            i = Intent(this@MainActivity, SearchActivity::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.slide_in_bottom_fade, R.anim.slide_out_top_fade);
        }
        binding.appBarMain.power.setOnClickListener {
            try {
                if (!serviceBound) {
                    flag_exit = true
                    playAudio()
                } else {
                    player!!.stopSelf()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            finish()
        }

        /*
                setData()

                binding.sideNavFooter.navRecyclerView.setHasFixedSize(true)
                val genreAdapter = GenresAdapter(this@MainActivity, radioDataModel, this@MainActivity)
                binding.sideNavFooter.navRecyclerView.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
                binding.sideNavFooter.navRecyclerView.adapter = genreAdapter

                recyclerView.setHasFixedSize(true)
                val adapter = MainCatAdapter(this@MainActivity, radioDataModel, this@MainActivity)
                recyclerView.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
                recyclerView.adapter = adapter
        */

        binding.appBarMain.contentMain.mainBottomBar.root.setOnClickListener {
            val intent = Intent(this, RadioStationActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_bottom_fade, R.anim.slide_out_top_fade);

        }
        val str = Gson().toJson(Config.jsonArray)
        Log.e("str: ", str)
        try {
            Log.e("jsonObject: ", Config.jsonArray.toString())
            setData()
            val genreAdapter = GenresAdapter(this@MainActivity, radioDataModel, this@MainActivity)
            binding.sideNavFooter.navRecyclerView.layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            binding.sideNavFooter.navRecyclerView.adapter = genreAdapter

            binding.sideNavFooter.navRecyclerView.setHasFixedSize(true)
            val adapter = MainCatAdapter(this@MainActivity, radioDataModel, this@MainActivity)
            binding.appBarMain.contentMain.recyclerView.layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            binding.appBarMain.contentMain.recyclerView.adapter = adapter


        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            LogUtils.logException(TAG, "addValueEventListener", e)
        }


    }

    private fun initializeMobileAdsSdk() {
        // throw RuntimeException("Test Crash") // Force a crash throw RuntimeException("Test Crash") // Force a crash
        val testDeviceIds: List<String> = listOf("5660A47A6E7B28F1ABCFF49FA95C61C4","C0B31AE74793DCC354E50098BE39F4CD")
        val configuration = RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build()
        MobileAds.setRequestConfiguration(configuration)
        MobileAds.initialize(this) { status ->
            status.let {

            }

        }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()

            Config.exitedMain = true
        }
//        System.exit(0)
    }

    fun setData() {
        val n = Config.jsonArray.length()
        for (i in 0 until n) {
            val radioDataSingleModel = RadioDataModel()
            val jsonObject = Config.jsonArray.getJSONObject(i)

            radioDataSingleModel.catId = jsonObject.getInt("cat_id");
            radioDataSingleModel.catName = jsonObject.getString("cat_name")
            radioDataSingleModel.catList = jsonObject.getJSONArray("cat_list")
            radioDataModel.add(radioDataSingleModel)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
//        var i: Intent
        when (item.itemId) {
            /*R.id.nav_send -> {

            }*/
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun close() {
        binding.drawerLayout.closeDrawer(GravityCompat.START)
    }


    private var player: MediaPlayerService? = null
    internal var serviceBound = false
    private var activeAudio: CatListModel? = null

    override fun onRestart() {
        super.onRestart()
        if (!serviceBound) {
            playAudio()
        } else {
            try {
                if (player != null && player!!.isPlaying) {
                    binding.appBarMain.contentMain.mainBottomBar.root.visibility = View.VISIBLE
                    setupAudio()
                } else {
                    binding.appBarMain.contentMain.mainBottomBar.root.visibility = View.GONE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        /*try{
            serviceBound=false
            if(Config.fromRadio){
                Config.fromRadio=false
                binding.appBarMain.contentMain.mainBottomBar.root.visibility=View.VISIBLE
                setupAudio()
            }else if (player!=null && player!!.isPlaying){
                setDataMusic()
            }else if (Config.audioList!=null && player==null){
                setupAudio()
            }
            else{
                binding.appBarMain.contentMain.mainBottomBar.root.visibility=View.GONE
            }
        }catch (e: Exception){
            e.printStackTrace()
        }*/
    }

    override fun onDestroy() {
        super.onDestroy()
        /*if (serviceBound) {
            unbindService(serviceConnection)
            player!!.stopSelf()
        }*/

        val sharedPreferences = getSharedPreferences(Config.shared_pref_name, Activity.MODE_PRIVATE)
        sharedPreferences!!.edit().putInt("sleepTime", 0).apply()
    }

    private fun setupAudio() {
        MediaPlayerService.setInterface(this@MainActivity)
        if (Config.itemArray.length() > 0) {
            setDataMusic()
            //playAudio()
        }

        binding.appBarMain.contentMain.mainBottomBar.ivSongNext.setOnClickListener {
            if (player != null) {
                player!!.onSkipToNext()
                setDataMusic()
            }
        }

        if (player != null && player!!.isPlaying) {
            binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.setImageResource(R.drawable.ic_pause)
        } else {
            binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.setImageResource(R.drawable.ic_play)
        }

        binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.setOnClickListener(View.OnClickListener {
            if (player!!.isPlaying) {
                player!!.onPause()
                binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.setImageResource(
                    R.drawable.ic_play
                )
            } else {
                player!!.onPlay()
                binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.setImageResource(
                    R.drawable.ic_pause
                )
            }
        })
        //
    }

    private fun setDataMusic() {
        try {
            activeAudio = Config.audioList!![Config.index]
            val transformation = RoundedTransformationBuilder()
                .borderColor(Color.parseColor("#8c8c8c"))
                .borderWidthDp(1f)
                .cornerRadiusDp(10f)
                .oval(false)
                .build()

//           Picasso.Builder(this@MainActivity).build()
            Picasso.get()
                .load(activeAudio!!.chImage)
                .placeholder(R.drawable.rescueimg6)
                .error(R.drawable.rescueimg6)
                .fit()
                .transform(transformation)
                .into(binding.appBarMain.contentMain.mainBottomBar.ivSongImage)

            binding.appBarMain.contentMain.mainBottomBar.tvSongName.text = activeAudio?.chName
            binding.appBarMain.contentMain.mainBottomBar.ivLoading.visibility = View.GONE
            binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.visibility =
                View.VISIBLE
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun playAudio() {
        //Check is service is active
        if (!serviceBound) {
            val playerIntent = Intent(this, MediaPlayerService::class.java)
            //startService(playerIntent)
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE)
        } /*else {
            //Service is active
            //Send a broadcast to the service -> PLAY_NEW_AUDIO
            val broadcastIntent = Intent(Config.Broadcast_PLAY_NEW_AUDIO)
            sendBroadcast(broadcastIntent)
        }*/
    }

    //Binding this Client to the AudioPlayer Service
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as MediaPlayerService.LocalBinder
            player = binder.service
            serviceBound = true

            if (flag_exit) {
                player!!.stopSelf()
                return
            }

            if (player!!.isPlaying) {
                binding.appBarMain.contentMain.mainBottomBar.root.visibility = View.VISIBLE
            } else {
                binding.appBarMain.contentMain.mainBottomBar.root.visibility = View.GONE
            }
            try {
                setupAudio()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            serviceBound = false
        }

    }

    override fun play() {
        binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.setImageResource(R.drawable.ic_play)
    }

    override fun pause() {
        binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.setImageResource(R.drawable.ic_pause)
    }

    override fun next() {
        setDataMusic()
    }

    override fun prev() {
        setDataMusic()
    }

    override fun buffering() {
        binding.appBarMain.contentMain.mainBottomBar.ivLoading.visibility = View.VISIBLE
        binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.visibility = View.GONE
    }

    override fun stopBuffering() {
        binding.appBarMain.contentMain.mainBottomBar.ivLoading.visibility = View.GONE
        binding.appBarMain.contentMain.mainBottomBar.ivSongInfoOptionsToggle.visibility =
            View.VISIBLE
    }

    override fun startServiceFromMain() {
        val playerIntent = Intent(this, MediaPlayerService::class.java)
        startService(playerIntent)
    }

    override fun error() {

    }

    /*override fun stop() {
        binding.appBarMain.contentMain.mainBottomBar.root.visibility=View.GONE
        val pid = android.os.Process.myPid()
        android.os.Process.killProcess(pid)
    }*/
}



