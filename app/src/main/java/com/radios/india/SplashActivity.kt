package com.radios.india

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.radios.india.utils.Config
import org.json.JSONObject
import java.io.*

val CACHE_TIME_MILLIS = 60000 //1 day millis

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_new)
        var last = getLastModifiedTime()
        if (last == 0L) {
            writeToFile(System.currentTimeMillis().toString(), "time.txt")
            last = System.currentTimeMillis()
        }
        val diff = System.currentTimeMillis() - last;
        var fromCache: String? = null
        if (diff in 10001 until CACHE_TIME_MILLIS) {
            fromCache = loadFromCache()
        } else {
            val file = File(cacheDir, "music.txt")
            file.delete()
            writeToFile(System.currentTimeMillis().toString(), "time.txt")
        }
        if (fromCache == null || fromCache.isEmpty()) {
            val queue = Volley.newRequestQueue(this)
            val url = "https://shortfilmsite.com/json/marathiradio.json"
            val stringRequest = StringRequest(Request.Method.GET, url, listener) {
                println("That didn't work!") //retry
                val altReq = StringRequest(Request.Method.GET, "http://radiosindiaapp.com/json/marathiradio.json", listener) {

                }
                altReq.setShouldCache(false)
                queue.add(altReq)

            } // Add the request to the RequestQueue.
            stringRequest.setShouldCache(false)
            queue.add(stringRequest)

        } else {
            handleResponse(fromCache)
        }
    }

    val listener: Response.Listener<String> = Response.Listener<String> { response ->
        try {
            handleResponse(response)
            writeToFile(response, "music.txt")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun loadFromCache(): String? {
        return readFromFile()
    }

    private fun handleResponse(response: String) {
        val jsnObject = if (BuildConfig.DEBUG) {
            val inputStream = assets.open("radios.json")
            val size: Int = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            val string = String(buffer)
            JSONObject(string)
        } else {
            JSONObject(response)
        }
        Config.jsonArray = (jsnObject.getJSONArray("radio_data"))
        Log.e("jsonObject: ", Config.jsonArray.toString())
        val intent = Intent(this@SplashActivity, MainActivity::class.java)

        startActivity(intent)
        finish()
    }

    private fun writeToFile(data: String, name: String) {
        try {
            val file = File(cacheDir, name)
            val outputStreamWriter = FileWriter(file)
            outputStreamWriter.write(data)
            outputStreamWriter.close()
        } catch (e: IOException) {
            Log.e("Exception", "File write failed: $e")
        }
    }

    private fun getLastModifiedTime(): Long {
        val file = File(cacheDir, "time.txt")
        if (!file.exists()) {
            file.createNewFile()
        }
        val data = readFileContent(file)
        if (data.isNotEmpty()) {
            return data.trim().toLong()
        }
        return 0;
    }

    private fun readFromFile(): String? {
        var ret: String? = null
        try {
            val file = File(cacheDir, "music.txt")
            if (file.exists()) {
                ret = readFileContent(file)
            }

        } catch (e: FileNotFoundException) {
            Log.e("login activity", "File not found: " + e.toString())
        } catch (e: IOException) {
            Log.e("login activity", "Can not read file: " + e.toString())
        }
        return ret
    }

    private fun readFileContent(file: File): String {
        val ret1: String?
        val inputStream: InputStream = FileInputStream(file)

        val inputStreamReader = InputStreamReader(inputStream)
        val bufferedReader = BufferedReader(inputStreamReader)
        var receiveString: String? = ""
        val stringBuilder = StringBuilder()
        while (bufferedReader.readLine().also { receiveString = it } != null) {
            stringBuilder.append("\n").append(receiveString)
        }
        inputStream.close()
        ret1 = stringBuilder.toString()
        return ret1
    }
}
